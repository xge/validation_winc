
from numpy import nan

xpoints  = [-0.5, 0.5]
xedges   = [-1.0, 0.0, 1.0]
xmins    = [-1.0, 0.0]
xmaxs    = [0.0, 1.0]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [0.0, 56993.14],
  'curve1' : [0.0, 57080.23],
}
yedges = {
  'curve0' : [0.0, 0.0, 56993.14],
  'curve1' : [0.0, 0.0, 57080.23],
}
yups = {
  'curve0' : [0.0, 83.21552138874094],
  'curve1' : [0.0, 81.84363139548489],
}
ydowns = {
  'curve0' : [0.0, 83.21552138874094],
  'curve1' : [0.0, 81.84363139548489],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0, 1.0],
  'curve1' : [1.0, 1.0, 1.0015280786424472],
}
ratio0_ymax = {
  'curve0' : [1.0, 1.0014600971518457],
  'curve1' : [1.0, 1.002964104651814],
}
ratio0_ymin = {
  'curve0' : [1.0, 0.9985399028481544],
  'curve1' : [1.0, 1.0000920526330803],
}
ratio0_yerrs = {
  'curve0' : [
    [0.0, 0.0014600971518455852],
    [0.0, 0.0014600971518456962],
  ],
  'curve1' : [
    [0.0, 0.0014360260093668753],
    [0.0, 0.0014360260093668753],
  ],
}
ratio0_variation_vals = {
}