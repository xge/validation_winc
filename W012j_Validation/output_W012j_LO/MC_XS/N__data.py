
from numpy import nan

xpoints  = [0.5]
xedges   = [0.0, 1.0]
xmins    = [0.0]
xmaxs    = [1.0]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [485699.0],
  'curve1' : [486409.0],
}
yedges = {
  'curve0' : [485699.0, 485699.0],
  'curve1' : [486409.0, 486409.0],
}
yups = {
  'curve0' : [696.9210859200631],
  'curve1' : [697.4302832541758],
}
ydowns = {
  'curve0' : [696.9210859200631],
  'curve1' : [697.4302832541758],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0],
  'curve1' : [1.0014618107099253, 1.0014618107099253],
}
ratio0_ymax = {
  'curve0' : [1.0014348826864377],
  'curve1' : [1.0028977417768086],
}
ratio0_ymin = {
  'curve0' : [0.9985651173135623],
  'curve1' : [1.000025879643042],
}
ratio0_yerrs = {
  'curve0' : [
    [0.0014348826864376552],
    [0.0014348826864376552],
  ],
  'curve1' : [
    [0.0014359310668834446],
    [0.0014359310668832226],
  ],
}
ratio0_variation_vals = {
}