
from numpy import nan

xpoints  = [-0.5, 0.5]
xedges   = [-1.0, 0.0, 1.0]
xmins    = [-1.0, 0.0]
xmaxs    = [0.0, 1.0]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [0.0, 56433.21],
  'curve1' : [0.0, 56358.97],
}
yedges = {
  'curve0' : [0.0, 0.0, 56433.21],
  'curve1' : [0.0, 0.0, 56358.97],
}
yups = {
  'curve0' : [0.0, 73.16700759222014],
  'curve1' : [0.0, 71.89252395068628],
}
ydowns = {
  'curve0' : [0.0, 73.16700759222014],
  'curve1' : [0.0, 71.89252395068628],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0, 1.0],
  'curve1' : [1.0, 1.0, 0.9986844625708869],
}
ratio0_ymax = {
  'curve0' : [1.0, 1.0012965239367426],
  'curve1' : [1.0, 0.9999584025780331],
}
ratio0_ymin = {
  'curve0' : [1.0, 0.9987034760632574],
  'curve1' : [1.0, 0.9974105225637406],
}
ratio0_yerrs = {
  'curve0' : [
    [0.0, 0.0012965239367426218],
    [0.0, 0.0012965239367426218],
  ],
  'curve1' : [
    [0.0, 0.0012739400071463125],
    [0.0, 0.0012739400071462015],
  ],
}
ratio0_variation_vals = {
}