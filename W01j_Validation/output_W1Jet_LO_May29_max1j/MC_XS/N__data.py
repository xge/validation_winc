
from numpy import nan

xpoints  = [0.5]
xedges   = [0.0, 1.0]
xmins    = [0.0]
xmaxs    = [1.0]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [615714.0],
  'curve1' : [614552.0],
}
yedges = {
  'curve0' : [615714.0, 615714.0],
  'curve1' : [614552.0, 614552.0],
}
yups = {
  'curve0' : [784.6744547900104],
  'curve1' : [783.9336706635326],
}
ydowns = {
  'curve0' : [784.6744547900104],
  'curve1' : [783.9336706635326],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0],
  'curve1' : [0.9981127601451323, 0.9981127601451323],
}
ratio0_ymax = {
  'curve0' : [1.0012744138590157],
  'curve1' : [0.9993859708739179],
}
ratio0_ymin = {
  'curve0' : [0.9987255861409844],
  'curve1' : [0.9968395494163467],
}
ratio0_yerrs = {
  'curve0' : [
    [0.0012744138590156107],
    [0.0012744138590157217],
  ],
  'curve1' : [
    [0.001273210728785612],
    [0.001273210728785612],
  ],
}
ratio0_variation_vals = {
}