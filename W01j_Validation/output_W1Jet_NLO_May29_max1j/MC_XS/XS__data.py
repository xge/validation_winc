
from numpy import nan

xpoints  = [0.0]
xedges   = [-0.5, 0.5]
xmins    = [-0.5]
xmaxs    = [0.5]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [0.0],
  'curve1' : [0.0],
}
yedges = {
  'curve0' : [0.0, 0.0],
  'curve1' : [0.0, 0.0],
}
yups = {
  'curve0' : [0.0],
  'curve1' : [0.0],
}
ydowns = {
  'curve0' : [0.0],
  'curve1' : [0.0],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0],
  'curve1' : [1.0, 1.0],
}
ratio0_ymax = {
  'curve0' : [1.0],
  'curve1' : [1.0],
}
ratio0_ymin = {
  'curve0' : [1.0],
  'curve1' : [1.0],
}
ratio0_yerrs = {
  'curve0' : [
    [0.0],
    [0.0],
  ],
  'curve1' : [
    [0.0],
    [0.0],
  ],
}
ratio0_variation_vals = {
}