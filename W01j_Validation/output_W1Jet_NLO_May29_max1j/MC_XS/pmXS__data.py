
from numpy import nan

xpoints  = [-0.5, 0.5]
xedges   = [-1.0, 0.0, 1.0]
xmins    = [-1.0, 0.0]
xmaxs    = [0.0, 1.0]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [12930.37, 78426.02],
  'curve1' : [12619.8, 77752.7],
}
yedges = {
  'curve0' : [12930.37, 12930.37, 78426.02],
  'curve1' : [12619.8, 12619.8, 77752.7],
}
yups = {
  'curve0' : [48.09984407459134, 118.3450886179904],
  'curve1' : [45.02563714152194, 111.76130815268762],
}
ydowns = {
  'curve0' : [48.09984407459134, 118.3450886179904],
  'curve1' : [45.02563714152194, 111.76130815268762],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0, 1.0],
  'curve1' : [0.97598135242843, 0.97598135242843, 0.991414584088291],
}
ratio0_ymax = {
  'curve0' : [1.0037199124290017, 1.0015090028617797],
  'curve1' : [0.9794635139707155, 0.9928396380200435],
}
ratio0_ymin = {
  'curve0' : [0.9962800875709983, 0.9984909971382203],
  'curve1' : [0.9724991908861446, 0.9899895301565386],
}
ratio0_yerrs = {
  'curve0' : [
    [0.003719912429001715, 0.0015090028617796847],
    [0.003719912429001715, 0.0015090028617796847],
  ],
  'curve1' : [
    [0.0034821615422854224, 0.001425053931752407],
    [0.0034821615422855334, 0.001425053931752518],
  ],
}
ratio0_variation_vals = {
}