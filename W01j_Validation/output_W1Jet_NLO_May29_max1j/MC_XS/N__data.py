
from numpy import nan

xpoints  = [0.5]
xedges   = [0.0, 1.0]
xmins    = [0.0]
xmaxs    = [1.0]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [530475.0],
  'curve1' : [562560.0],
}
yedges = {
  'curve0' : [530475.0, 530475.0],
  'curve1' : [562560.0, 562560.0],
}
yups = {
  'curve0' : [728.3371472058802],
  'curve1' : [750.0399989333902],
}
ydowns = {
  'curve0' : [728.3371472058802],
  'curve1' : [750.0399989333902],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0],
  'curve1' : [1.0604835289127668, 1.0604835289127668],
}
ratio0_ymax = {
  'curve0' : [1.0013729905220903],
  'curve1' : [1.0618974315451877],
}
ratio0_ymin = {
  'curve0' : [0.9986270094779097],
  'curve1' : [1.059069626280346],
}
ratio0_yerrs = {
  'curve0' : [
    [0.0013729905220902783],
    [0.0013729905220902783],
  ],
  'curve1' : [
    [0.0014139026324206938],
    [0.0014139026324209159],
  ],
}
ratio0_variation_vals = {
}