
from numpy import nan

xpoints  = [-0.5, 0.5]
xedges   = [-1.0, 0.0, 1.0]
xmins    = [-1.0, 0.0]
xmaxs    = [0.0, 1.0]
xerrs = [
  [abs(xpoints[i] - xmins[i])   for i in range(len(xpoints))],
  [abs(xmaxs[i]   - xpoints[i]) for i in range(len(xpoints))]
]

yvals = {
  'curve0' : [75134.0, 455341.0],
  'curve1' : [78557.0, 484003.0],
}
yedges = {
  'curve0' : [75134.0, 75134.0, 455341.0],
  'curve1' : [78557.0, 78557.0, 484003.0],
}
yups = {
  'curve0' : [274.1058189823777, 674.7895968374143],
  'curve1' : [280.28021692584724, 695.703241332107],
}
ydowns = {
  'curve0' : [274.1058189823777, 674.7895968374143],
  'curve1' : [280.28021692584724, 695.703241332107],
}
variation_yvals = {
}


# lists for ratio plot
ratio0_yvals = {
  'curve0' : [1.0, 1.0, 1.0],
  'curve1' : [1.04555860196449, 1.04555860196449, 1.0629462315056188],
}
ratio0_ymax = {
  'curve0' : [1.0036482260891524, 1.0014819434156763],
  'curve1' : [1.049289006534004, 1.0644741045531416],
}
ratio0_ymin = {
  'curve0' : [0.9963517739108475, 0.9985180565843238],
  'curve1' : [1.0418281973949763, 1.061418358458096],
}
ratio0_yerrs = {
  'curve0' : [
    [0.0036482260891524954, 0.0014819434156762012],
    [0.0036482260891523843, 0.0014819434156763123],
  ],
  'curve1' : [
    [0.003730404569513679, 0.0015278730475227764],
    [0.003730404569513901, 0.0015278730475227764],
  ],
}
ratio0_variation_vals = {
}